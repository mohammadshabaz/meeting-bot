var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Meeting Bot' });
});

router.get('/auth', function(req, res) {
   var constants = require('../common/constants');
   var request = require('request');
   if (!req.query.code) {
    var authorizeUrl = constants.authorizeUrl 
        + '?response_type=code' 
        + '&client_id=' + constants.appId 
        + '&resource=' + encodeURIComponent(constants.resource)
        + '&redirect_uri=' + encodeURIComponent(constants.redirectUri); 
        // + '&scope=' + encodeURIComponent(constants.scope);
        //+ '&state=a5d02376-dcbd-4eac-9d42-9092503a9cec'
        
        res.send({
            authorizeUrl: authorizeUrl
        });
   } else {
       var tokenUrl = constants.tokenUrl; 
        var data = {
                grant_type:'authorization_code',
                redirect_uri:constants.redirectUri,
                client_id:constants.appId,
                client_secret:constants.appSecret,
                code:req.query.code,
                resource:'https://graph.microsoft.com/'
        };
        request.post({
            url: tokenUrl,
            form:data
        }, function(err, response, body) {
            res.send(JSON.parse(body));
        });
   }
});

router.get('/token', function(req, res) {
   var constants = require('../common/constants');
   var request = require('request');
   var tokenUrl = constants.tokenUrl; 
   var data = {'grant_type':'authorization_code',
        redirect_uri:constants.redirectUri,
        client_id:constants.appId,
        client_secret:constants.appSecret,
        code:req.query.code,
        resource:'https://graph.microsoft.com/'
   };
    request.post({
        url: tokenUrl,
        form:data
    }, function(err, response, body) {
        res.send(JSON.parse(body));
    });
});

module.exports = router;
