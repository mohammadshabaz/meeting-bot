var express = require('express');
var router = express.Router();

/*
    Returns 10 events upto next day 
 GET events listing. */
router.get('/events', function(req, res) {
    var apiHelper = require('../common/apiHelper');
    var constants = require('../common/constants')
    var endpoint = '/me/events';
    // assign default now + x day enddate
    var now = new Date();
    var startDateTime = now.toISOString();
    now.setDate(now.getDate() + constants.defaultDateInterval); // push by x day
    var endDateTime = now.toISOString();
    // check for start date and append optional enddate
    if (!!req.query.startDateTime) {
        startDateTime = req.query.startDateTime;
        if (!!req.query.endDateTime) {
            endDateTime = req.query.endDateTime;
        }
    }
    
    // setup data
    var data = {
        StartDateTime: startDateTime,
        EndDateTime: endDateTime,
        '$top': 10,
        '$select': 'createdDateTime,subject,body,bodyPreview,location,attendees,organizer,webLink,start,end'
    };
    
    console.log(data);
    
    apiHelper.request(
        endpoint,
        'GET',
        data,
        function(data) {
            res.send(data);
        },
        function(error) {
            res.send(error);
        }
    ) 
});

router.get('/find', function(req, res) {
    var utils = require('../common/utils');
    var name = req.query.name;
    var type = req.query.type || 'users';  
    utils.find(name, type, function(data) {
        console.log(data);
       if (!!data) {
           res.send(data);
       } else {
           res.status(404).send({
            message: 'No results'
           });
       }
    });
    
});

// schedule a 'tech talk' with 'raju' (at 10AM default)
// (expected: timezone: )

// expected inputs title*, participant*, startDateTime (default tomorrow 10 AM), timezone (default PST)
/* Sample input
{
    "title":"Tech Talk",
    "timezone": "India Standard Time",
    "startDateTime": "2016-02-02T12:00:00",
    "endDateTime": "2016-02-02T13:00:00",
    "attendee": "raju"
}
*/
router.post('/events', function(req, res) {
    var utils = require('../common/utils');
    var apiHelper = require('../common/apiHelper');
    var endpoint = '/me/events';
    
    console.log('body is ', req.body);
    // check for required fields
    if (!utils.hasKeys(req.body, ['title', 'attendee'])) {
        res.status(400).send({
            message:'Not enough info'
        });
    }
    utils.getMeetingData(req.body, function(data) {
        if (data == null) {
            res.status(400).send({
               message: 'Invalid attendee' 
            });
            return;
        } 
        
         apiHelper.request(
            endpoint,
            'POST',
            data,
            function(data) {
                res.send(data);
            },
            function(error) {
                res.send(error);
            }
        );
    });
});

module.exports = router;
