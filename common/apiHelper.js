module.exports = {
    request: function(endpoint, method, data, successCb, errorCb) {
        var constants = require('./constants'),
            request = require('request'),
            utils = require('./utils');
        var url = constants.apiBaseUrl + endpoint;
        var requestOptions = {
            url: url,
            method: method,
            headers: {
                Authorization: 'Bearer ' + constants.access_token,
                Accept: 'application/json; odata.metadata=none'
            }
        };
        
        if (method == 'GET') {
            requestOptions.url += utils.getQueryString(data);
        } else {
            requestOptions.body = data;
            requestOptions.json = true;
        }
        
        console.log('Making request to ' + url);
        console.log('With data ', data);
        
        request(requestOptions, function(error, response, body) {
            if (!error && response.statusCode < 300) {
                console.log('Response is ' + response.statusCode);
                try {
                    if (body.length > 0) {
                        var json = JSON.parse(body);
                        successCb(json);
                    } else {
                        successCb(body);
                    }
                } catch(e) {
                    successCb(body);
                }
            } else {
                errorCb(response);
            }
        });
    }  
};