module.exports = {
   self: this,
    
    getQueryString: function(json) {
        return '?' + 
        Object.keys(json).map(function(key) {
            return encodeURIComponent(key) + '=' +
                encodeURIComponent(json[key]);
        }).join('&');
    },
    
    hasKeys: function(object, keys) {
        console.log('Received ', object);
      for (var index in keys) {
          if (!object[keys[index]]) {
              console.log('Missing ', keys[index]);
              return false;
          }
      }
      return true;  
    },
    
    getStartDate: function(requestBody) {
        if (!!requestBody.startDateTime) {
            return requestBody.startDateTime;
        }
        var date = new Date();
        date.setDate(date.getDate() + 1);
        date.setHours(10);
        date.setMinutes(0);
        date.setSeconds(0);
        return date.toISOString();
    },
    
    getEndDate: function(requestBody, startDate) {
        if (!!requestBody.endDateTime) {
            return requestBody.endDateTime;
        }
        var endDate = new Date(Date.parse(startDate));
        endDate.setHours(endDate.getHours() + 1);
        endDate.setMinutes(0);
        endDate.setSeconds(0);        
        return endDate.toISOString();
    },
    
    findAttendees: function(requestBody, cb) {
        var utils = require('./utils');
        var name = requestBody.attendee;
        utils.find(name, 'users', function(data) {
            if (!!data && !!data.value && data.value.length != 0) {
                var attendee = data.value[0];
                cb(utils.getAttendeeModel(attendee));
                return;
            }
            
            console.log('find group');
            // user not find, try groups
            utils.find(name, 'groups', function(data) {
                console.log(data);
                if (!!data && !!data.value && data.value.length != 0) {
                    var attendee = data.value[0];
                    cb(utils.getAttendeeModel(attendee));
                    return;
                }
                // no one found :(
                cb(null); 
            });
            
        })
        
    },
    
    getAttendeeModel: function(attendee) {
        return [{
            "EmailAddress": {
                "Address": attendee.mail,
                "Name": attendee.displayName
            },
            "Type": "Required"
            }];
    },
    
    getMeetingData: function(requestBody, callback) {
        var utils = require('./utils');
        var startDateTime = utils.getStartDate(requestBody);
        var endDateTime = utils.getEndDate(requestBody, startDateTime);
        var timezone = requestBody.timezone || 'Pacific Standard Time';
        utils.findAttendees(requestBody, function(attendees) {
            if (attendees == null) {
                callback(null);
                return;
            }
           var meeting = {
                Subject: requestBody.title,
                Start: {
                    DateTime: startDateTime,
                    TimeZone: timezone
                },
                End: {
                    DateTime: endDateTime,
                    TimeZone: timezone
                },
                Attendees: attendees
            };
            // return callback with meeting object
            callback(meeting)    
        });
    },
    
    find: function(prefix, type, cb) {
        var apiHelper = require('../common/apiHelper');
        var name = prefix;
        var endpoint = '/' + type;
        var data = {
            '$filter': 'startswith(displayName, \'' + encodeURIComponent(name) + '\')'  
        };
        
        apiHelper.request(
            endpoint,
            'GET',
            data,
            function(data) {
                cb(data);
            },
            function(error) {
                cb(null);
            }
        ) 
    }
};